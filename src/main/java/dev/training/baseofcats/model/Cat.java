/**
 * 
 */
package dev.training.baseofcats.model;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author artisticImpresion 17/02/2019
 *
 */
public class Cat {
  private String name;
  private Date dateOfBirth = null;
  private Float weight;
  private String guardiansName;
  
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Date getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public Float getWeight() {
    return weight;
  }

  public void setWeight(Float weight) {
    this.weight = weight;
  }

  public String getGuardiansName() {
    return guardiansName;
  }

  public void setGuardiansName(String guardiansName) {
    this.guardiansName = guardiansName;
  }

  public void introduceYourself() {
    System.out.println("My name is: " + this.name + "; I was born in: " + returnAsSimpleDate(this.dateOfBirth) + "; My weight is: "
        + this.weight + "; My guardian's name is: " + this.guardiansName + ";");
  }
  
  public String returnAsSimpleDate(Date dateToFormat) {
    String pattern = "yyyy-MM-dd";
    String result = "-";
    SimpleDateFormat sdf = new SimpleDateFormat(pattern);
    if(dateToFormat!=null) {
      result = sdf.format(dateToFormat);  
    }
    return result;
  }
  
}
