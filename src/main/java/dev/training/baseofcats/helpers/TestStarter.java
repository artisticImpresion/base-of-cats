/**
 * 
 */
package dev.training.baseofcats.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;
import java.util.function.IntPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dev.training.baseofcats.model.Cat;
import dev.training.baseofcats.service.CatDAO;

/**
 * @author artisticImpresion 17/02/2019
 *
 */
public class TestStarter {

  public static Scanner sc = new Scanner(System.in);
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    CatDAO catDAO = new CatDAO();
    menuRunner(catDAO);
  }
  
  private static void createCat(CatDAO catDAO) {
    Cat cat = new Cat();
    System.out.println("Enter cat name: ");
    String nameReadFromUser = getUserInput();
    
    System.out.println("Enter guardian name: ");
    String guardiansName = getUserInput();
    
    String dateReadFromUser = null;
    String datePattern = "yyyy-MM-dd";
          
    do {
      System.out.println(String.format("Enter cat's DOB in format %s: ", datePattern));
      try {
        dateReadFromUser = getUserInput();
      } catch (Exception e) {
        e.printStackTrace();
      }
    } while(!Pattern.matches("[0-9]{4,4}\\-[0-9]{2,2}\\-[0-9]{2,2}", dateReadFromUser));
    
    SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
    Date date = null;
    try {
      date = sdf.parse(dateReadFromUser);
    } catch (ParseException e) {
      System.out.println("Something went wrong while converting inputed DOB!");
    }
    
    String weightRegex = "[0-9]{1,2}\\.[0-9]{0,2}";
    String weightRegex2 = "[0-9]{1,2}\\,[0-9]{0,2}";
    //TODO weightRegex2 will be used to convert 12,03 to 12.03;
    
    Pattern weightPattern = Pattern.compile(weightRegex);
    do {
      System.out.println("Enter cat's weight: ");
      //inputWeight(cat);
      String inputWeight = getUserInput();
      Matcher weightMatcher = weightPattern.matcher(inputWeight);
      if(weightMatcher.matches()) {
        try {
//          Float weight = new Float(inputWeight);
          Float weight = Float.valueOf(inputWeight);
          cat.setWeight(weight);
        } catch (NumberFormatException e) {
          System.err.println("Invalid value, unable to save it!");
        }  
      } else {
        System.out.println("Please input value in format like '92.85'!");
      }
    } while (cat.getWeight()==null);
    
    cat.setName(nameReadFromUser);
    cat.setGuardiansName(guardiansName);
    cat.setDateOfBirth(date);
    cat.introduceYourself();
    
    catDAO.addCat(cat);
  }
  
//  private static Cat inputWeight(Cat cat) {
//    String catsWeight = getUserInput();
//    return validateWeight(getUserInput(), cat);
//  }

//  private static Cat validateWeight(String catsWeight, Cat cat) {
//    try {
//      Float weight = new Float(catsWeight);
//      cat.setWeight(weight);
//      return cat;
//    } catch (NumberFormatException e) {
//      System.err.println("Invalid value, unable to save it!");
//      return null;
//    }
//  }

  public static String getUserInput() {
    return sc.nextLine().trim();
  }

  public static void menuRunner(CatDAO catDAO) {
    String readOption;
    readOption = "0";
    while (!"X".equals(readOption.toUpperCase())) {
      switch (readOption) {
      case "0":
        showOptions();
        break;
      case "1":
        createCat(catDAO);
        showOptions();
        break;
      case "2":
        displayAllCats(catDAO);
        moreInfoMock(catDAO);
        showOptions();
        break;
      default:
        System.out.println("Please choose an option!");
        break;
      }
      readOption = getUserInput();
    }
    System.out.println("Bye!");
  }
  
  private static void moreInfoMock(CatDAO catDAO) {
    String readOption = "";
    while (!"Z".equals(readOption.toUpperCase())) {
      System.out.println("Press 'z' to go back to main menu.");
      readOption = getUserInput();
      Integer intOption;
      try {
        intOption = Integer.parseInt(readOption);
        Cat cat = getCat(catDAO, intOption);
        if(null!=cat) {
          cat.introduceYourself();
        }
      } catch (Exception e) {
        System.err.println("Number expected!");
      }
    }
  }

  private static Cat getCat(CatDAO catDAO, Integer intOption) {
    if(!catDAO.getCatList().isEmpty()){
      if(intOption>0) {
        try {
          Cat cat = catDAO.getCatList().get(intOption - 1);
          return cat;
        } catch (IndexOutOfBoundsException e) {
          System.err.println("Unable to find cat number " + intOption);
        }
      } else {
        System.err.println("Positive number expected!");
      }
    } else {
      System.err.println("Sorry, cat list is empty!");
    }
    return null;
  }

  private static void displayAllCats(CatDAO catDAO) {
    if(catDAO.getCatList().isEmpty()) {
      System.out.println("Nothing to display!");
    } else {
      for (Cat cat : catDAO.getCatList()) {
        System.out.println(catDAO.getCatList().indexOf(cat) + 1 + ". " + cat.getName());
      }
    }
  }

  private static void showOptions() {
    System.out.println("Please choose menu option:");
    System.out.println("0. Show options.");
    System.out.println("1. Add cat.");
    System.out.println("2. Display all cats.");
    System.out.println("x Exit application.");
  }
  
}
