/**
 * 
 */
package dev.training.baseofcats.helpers;

import org.apache.commons.lang3.StringUtils;

/**
 * @author artisticImpresion 02/04/2019
 *
 */
public class Utils {
  
  public static char charToUpperCase(char charToConvert) {
    Character newChar = charToConvert;
    String newString = "";
    newString = newString + newChar;
    newString = StringUtils.capitalize(newString);
    char resultChar = newString.charAt(0);
    return resultChar;
  }
  
}
