/**
 * 
 */
package dev.training.baseofcats.service;

import java.util.ArrayList;

import dev.training.baseofcats.model.Cat;

/**
 * @author artisticImpresion 20/03/2019
 *
 */
public class CatDAO {
  
  private ArrayList<Cat> catList = new ArrayList<Cat>();
  
  public ArrayList<Cat> getCatList() {
    return catList;
  }

  public void setCatList(ArrayList<Cat> catList) {
    this.catList = catList;
  }

  public void addCat(Cat cat) {
    this.getCatList().add(cat);
    System.out.println("Thanks, cat was added to collection!");
  }
}
